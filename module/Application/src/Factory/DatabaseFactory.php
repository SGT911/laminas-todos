<?php
declare(strict_types=1);

namespace Application\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;

class DatabaseFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) 
    {
        $dbAdapter = $container->get(AdapterInterface::class);
        return new $requestedName($dbAdapter);
    }
}
