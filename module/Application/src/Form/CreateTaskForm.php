<?php

declare(strict_types=1);

namespace Application\Form;

use Laminas\Filter;
use Laminas\Validator;
use Laminas\Form\Element as LaminasElement;
use Laminas\Form\Form;
use Laminas\Form\FormInterface;
use Laminas\InputFilter;


class AsNull {
    function __invoke(?string $value): ?string
    {
        if ($value == null || strlen($value) > 0) return $value;
        return null;
    }
}

class CreateTaskForm extends Form {
    public function __construct() {
        parent::__construct();

        $filter = new InputFilter\InputFilter();
        $factory = $this->getFormFactory()->getInputFilterFactory();

        $filter->setFactory($factory);

        $filter->add($factory->createInput([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => Filter\StringTrim::class],
            ],
            'validators' => [
                ['name' => Validator\NotEmpty::class],
                [
                    'name' => Validator\StringLength::class,
                    'options' => [
                        'min' => 4,
                        'max' => 250,
                        'messages' => [
                            Validator\StringLength::TOO_LONG => 'The title is too long',
                            Validator\StringLength::TOO_SHORT => 'The title must be longer than 4 letters',
                        ],
                    ],
                ],
            ],
        ]));

        $filter->add($factory->createInput([
            'name' => 'security',
            'required' => true,
            'validators' => [
                Element\Security::getValidator(),
            ],
        ]));

        $filter->add($factory->createInput([
            'name' => 'description',
            'required' => true,
            'filters' => [
                ['name' => Filter\StringTrim::class],
                ['name' => Filter\StripNewlines::class],
                [
                    'name' => Filter\Callback::class,
                    'options' => [
                        'callback' => AsNull::class,
                    ],
                ],
            ],
        ]));

        $this->setInputFilter($filter);

        $this->setAttribute('method', 'POST');

        $this->add([
            'type' => LaminasElement\Text::class,
            'name' => 'name',
            'options' => [
                'label' => 'Title',
            ],
        ]);

        $this->add([
            'type' => LaminasElement\Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => 'Description',
            ],
        ]);

        $this->add([
            'type' => LaminasElement\Checkbox::class,
            'name' => 'completed',
            'options' => [
                'label' => 'Task is complete',
            ],
        ]);

        $this->add([
            'type' => Element\Security::class,
            'name' => 'security',
            'options' => [
                'timeout' => 120,
                'method' => 'POST',
            ],
        ]);

        $this->get('completed')->setValue(false);
    }
}