<?php

declare(strict_types=1);

namespace Application\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class SearchTaskForm extends Form {
    public function __construct() {
        parent::__construct();

        $this->setAttribute('method', 'GET');

        $this->add([
            'type' => Element\Text::class,
            'name' => 'name',
            'required' => false,
            'options' => [
                'label' => 'Search',
            ],
        ]);
    }
}