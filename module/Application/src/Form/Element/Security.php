<?php

declare(strict_types=1);

namespace Application\Form\Element;

use Laminas\Form\Element;
use Laminas\Json\Json;
use Laminas\Validator\ValidatorInterface;
use Laminas\Validator;
use Laminas\Filter\StringTrim;

use DateTime;
use DateInterval;

class Security extends Element
{
    public const DATE_FORMAT = "Y.m.d-H.i.s";

    protected $attributes = [
        'type' => 'hidden',
    ];

    public function getValue(): string
    {
        $timeout = (int)($this->options['timeout'] ?? 60 * 5);
        $requestMethod = $this->options['method'] ?? 'POST';
        $requestUrl = $this->options['url'] ?? $_SERVER['REQUEST_URI'];

        $data = Json::encode([
            'expireAt' => $this->nowAdd($timeout),
            'expectedMethod' => $requestMethod,
            'host' => $_SERVER['HTTP_HOST'],
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            'expectedUrl' => $requestUrl,
        ]);

        return base64_encode($data);
    }

    public static function getValidator(): ValidatorInterface
    {
        $validator = new Validator\ValidatorChain();

        $validator->attach(new Validator\NotEmpty());

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                return Security::decodeToken($value)['host'] == $_SERVER['HTTP_HOST'];
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'Token domain is not valid',
            ],
        ]));

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                return Security::decodeToken($value)['expectedMethod'] == $_SERVER['REQUEST_METHOD'];
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'Token method is not the expected',
            ],
        ]));

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                return Security::decodeToken($value)['expectedUrl'] == $_SERVER['REQUEST_URI'];
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'Token URL is not the expected',
            ],
        ]));

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                return Security::decodeToken($value)['userAgent'] == $_SERVER['HTTP_USER_AGENT'];
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'The browser client is not the expected',
            ],
        ]));

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                return Security::decodeToken($value)['userAgent'] == $_SERVER['HTTP_USER_AGENT'];
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'The browser client is not the expected',
            ],
        ]));

        $validator->attach(new Validator\Callback([
            'callback' => function($value) {
                $date = DateTime::createFromFormat(Security::DATE_FORMAT, Security::decodeToken($value)['expireAt']);

                $res = $date->diff(new DateTime());
                return $res->invert;
            },
            'messages' => [
                Validator\Callback::INVALID_VALUE => 'Token expired',
            ],
        ]));

        return $validator;
    }

    public static function decodeToken(string $token): array
    {
        return Json::decode(base64_decode($token), Json::TYPE_ARRAY);
    }

    public function nowAdd(int $timeout): string
    {
        $date = new DateTime();

        $date->add(DateInterval::createFromDateString("$timeout seconds"));

        return $date->format(Security::DATE_FORMAT);
    }
}
