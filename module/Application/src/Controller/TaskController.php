<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Form\CreateTaskForm;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Db\Adapter\Adapter as Database;
use Laminas\Form\FormInterface;
use Laminas\View\Model\ViewModel;
use ValueError;

class TaskController extends AbstractActionController
{

    private Database $db;
    protected const defaultSize = 10;

    function __construct(Database $db)
    {
        $this->db = $db;
    }

    private function getTask(int $id): ?array
    {
        $query = $this->db->query('SELECT
                CAST(rowid AS INT) AS id,
                name,
                description,
                completed,
                created_at
            FROM todos
                WHERE rowid = :id');

        $res = $query->execute(['id' => $id]);
        if (count($res)) {
            return $res->current();
        }

        return null;
    }

    private function updateTask(array $data, int $id) {
        foreach ($data as $k => $v) {
            $query = $this->db->query("UPDATE todos SET $k = :value WHERE rowid = :id");
            $query->execute([
                'value' => $v,
                'id' => $id,
            ]);
        }
    }

    private function deleteTask(int $id)
    {
        $query = $this->db->query('DELETE FROM todos
            WHERE rowid = :id');

        $res = $query->execute(['id' => $id]);
    }

    public function getAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        $task = $this->getTask($id);
        if ($task == null) {
            $this->getResponse()->setStatusCode(404);
            $view = new ViewModel([
                'exception' => new ValueError("The task ($id) does not exists"),
                'message' => "The task ($id) does not exists",
            ]);
            $view->setTemplate('error/404');
            return $view;
        }

        return new ViewModel([
            'task' => $task,
        ]);
    }

    public function updateAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        $task = $this->getTask($id);
        if ($task == null) {
            $this->getResponse()->setStatusCode(404);
            $view = new ViewModel([
                'exception' => new ValueError("The task ($id) does not exists"),
                'message' => "The task ($id) does not exists",
            ]);
            $view->setTemplate('error/404');
            return $view;
        }

        $form = new CreateTaskForm();

        $form->setData($task);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $form->setData($_POST);
            if ($form->isValid()) {
                $data = $form->getData(FormInterface::VALUES_AS_ARRAY);
                unset($data['security']);
                $this->updateTask($data, (int)$task['id']);
                return $this->redirect()->toRoute('task', ['id' => (int)$task['id']]);
            }
        }

        return new ViewModel([
            'task' => $task,
            'form' => $form,
        ]);
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        $task = $this->getTask($id);
        if ($task == null) {
            $this->getResponse()->setStatusCode(404);
            $view = new ViewModel([
                'exception' => new ValueError("The task ($id) does not exists"),
                'message' => "The task ($id) does not exists",
            ]);
            $view->setTemplate('error/404');
            return $view;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->deleteTask((int)$task['id']);
            return $this->redirect()->toRoute('tasks');
        }

        return new ViewModel([
            'task' => $task,
        ]);
    }

    public function toggleAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        $task = $this->getTask($id);
        if ($task == null) {
            $this->getResponse()->setStatusCode(404);
            $view = new ViewModel([
                'exception' => new ValueError("The task ($id) does not exists"),
                'message' => "The task ($id) does not exists",
            ]);
            $view->setTemplate('error/404');
            return $view;
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->updateTask(['completed' => !$task['completed']], (int)$task['id']);
            return $this->redirect()->toRoute('task', ['id' => (int)$task['id']]);
        }

        return new ViewModel([
            'task' => $task,
        ]);
    }
}