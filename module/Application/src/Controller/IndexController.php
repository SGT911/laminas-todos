<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Laminas\Db\Adapter\Adapter as Database;

class IndexController extends AbstractActionController
{

    private Database $db;

    function __construct(Database $db)
    {
        $this->db = $db;
    }

    private function dbVersion(): string {
        $res = $this->db->query('SELECT sqlite_version() AS version')->execute();
        return $res->current()['version'];
    }

    private function todoSummary(): array {
        $res = $this->db->query('SELECT COUNT(rowid) AS total, SUM(completed) AS complete FROM todos')->execute();
        $data = $res->current();
        if (!isset($data['total'])) $data['total'] = 0;
        if (!isset($data['complete'])) $data['complete'] = 0;
        return $data;
    }

    public function indexAction()
    {
        return new ViewModel([
            'dbVersion' => $this->dbVersion(),
            'todoSummary' => $this->todoSummary(),
            'phpVersion' => phpversion(),
            'laminasVersion' => '3.1.1',
        ]);
    }
}
