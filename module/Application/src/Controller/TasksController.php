<?php

declare(strict_types=1);

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Laminas\Db\Adapter\Adapter as Database;
use Application\Form;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Form\FormInterface;

class TasksController extends AbstractActionController
{

    private Database $db;
    protected const defaultSize = 10;

    function __construct(Database $db)
    {
        $this->db = $db;
    }

    private function createTask(string $name, string $description, string $completed) {
        $query = $this->db->query('INSERT INTO todos (name, description, completed) VALUES (:name, :description, :completed)');
        $query->execute([
            'name' => $name,
            'description' => $description,
            'completed' => $completed,
        ]);
    }

    private function countAllTodos(?int $size = null): int
    {
        if ($size == null) {
            $size = $this::defaultSize;
        }
        $query = $this->db->query('SELECT
            COUNT(rowid) AS count
        FROM todos');
        
        return (int)ceil($query->execute()->current()['count'] / $size);
    }

    private function countFilterTodos(string $search, ?int $size = null): int
    {
        if ($size == null) {
            $size = $this::defaultSize;
        }
        $query = $this->db->query('SELECT
            COUNT(rowid) AS count
        FROM todos
            WHERE name LIKE :search');
        
        return (int)ceil($query->execute([
            'search' => '%' . $search . '%',
        ])->current()['count'] / $size);
    }

    private function getAllTodos(int $page=1, ?int $size = null): ResultInterface
    {
        if ($size == null) {
            $size = $this::defaultSize;
        }
        $query = $this->db->query('SELECT
            rowid AS id,
            name AS title,
            description,
            completed,
            created_at
        FROM todos
            LIMIT :limit OFFSET :offset');
        
        return $query->execute([
            'limit' => $size,
            'offset' => (int)(($page - 1) * $size),
        ]);
    }

    private function getFilterTodos(string $search, int $page=1, ?int $size = null): ResultInterface
    {
        if ($size == null) {
            $size = $this::defaultSize;
        }
        $query = $this->db->query('SELECT
            rowid AS id,
            name AS title,
            description,
            completed,
            created_at
        FROM todos
            WHERE name LIKE :search
            LIMIT :limit OFFSET :offset');
        
        return $query->execute([
            'limit' => $size,
            'offset' => (int)(($page - 1) * $size),
            'search' => '%' . $search . '%',
        ]);
    }

    static public function createUrlParams(int $page): string
    {
        $get = $_GET;
        $get['page'] = $page;

        $url = '';
        foreach ($get as $k => $v) {
            $url .= urlencode($k) . "=" . urlencode("$v") . "&";
        }

        return '?' . substr($url, 0, -1);
    }

    public function listAction()
    {
        $form = new Form\SearchTaskForm();
        $get = $_GET;

        $hasForm = false;
        $page = 1;
        $size = null;

        if (array_key_exists('page', $get)) {
            $page = (int)$get['page'];
            unset($get['page']);
        }

        if (array_key_exists('size', $get)) {
            $size = (int)$get['size'];
            unset($get['size']);
        }

        if (count($get) > 0) {
            $form->setData($get);
            if ($form->isValid()) {
                $hasForm = true;
            }
        }

        if (!$hasForm) {
            $pages = $this->countAllTodos($size);
            if ($pages < $page) $page = 1;
            $data = $this->getAllTodos($page, $size);
        } else {
            $search = $form->getData(FormInterface::VALUES_AS_ARRAY)['name'];
            $pages = $this->countFilterTodos($search, $size);
            if ($pages < $page) $page = 1;
            $data = $this->getFilterTodos($search, $page, $size);
        }

        $urlParams = [];
        for ($i = 1; $i <= $pages; $i++) $urlParams[$i] = $this->createUrlParams($i);
        $nextUrl = $urlParams[$page + 1]?? null;
        $prevUrl = $urlParams[$page - 1]?? null;

        return new ViewModel([
            'form' => $form,
            'data' => $data,
            'page' => $page,
            'urlParams' => $urlParams,
            'nextUrl' => $nextUrl,
            'prevUrl' => $prevUrl,
        ]);
    }

    public function createAction()
    {
        $form = new Form\CreateTaskForm();
        $post = $_POST;

        if (count($post) > 0) {
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData(FormInterface::VALUES_AS_ARRAY);
                $this->createTask($data['name'], $data['description'], $data['completed']);
                return $this->redirect()->toRoute('tasks');
            }
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }
}
