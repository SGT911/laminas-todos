<?php

declare(strict_types=1);

namespace Application;

class Module
{
    public function getConfig(): array
    {
        /** @var array $config */
        $config = include __DIR__ . '/../config/module.config.php';
        return $config;
    }

    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'security' => Form\Element\Security::class
            )
        );
    }
}
