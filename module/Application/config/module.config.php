<?php

declare(strict_types=1);

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Session;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'tasks' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/ts[/:action]',
                    'defaults' => [
                        'controller' => Controller\TasksController::class,
                        'action'     => 'list',
                    ],
                ],
            ],
            'task' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/t[/:id[/:action]]',
                    'defaults' => [
                        'controller' => Controller\TaskController::class,
                        'action'     => 'get',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Factory\DatabaseFactory::class,
            Controller\TasksController::class => Factory\DatabaseFactory::class,
            Controller\TaskController::class  => Factory\DatabaseFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'            => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index'  => __DIR__ . '/../view/application/index/index.phtml',
            'application/tasks/list'   => __DIR__ . '/../view/application/tasks/list.phtml',
            'application/tasks/create' => __DIR__ . '/../view/application/tasks/create.phtml',
            'application/task/get'     => __DIR__ . '/../view/application/tasks/get.phtml',
            'application/task/update'  => __DIR__ . '/../view/application/tasks/update.phtml',
            'application/task/toggle'  => __DIR__ . '/../view/application/tasks/toggle.phtml',
            'application/task/delete'  => __DIR__ . '/../view/application/tasks/delete.phtml',
            'error/404'                => __DIR__ . '/../view/error/404.phtml',
            'error/index'              => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
