<?php

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Laminas\I18n',
    'Laminas\Validator',
    'Laminas\Session',
    'Laminas\Form',
    'Laminas\Db',
    'Laminas\InputFilter',
    'Laminas\Filter',
    'Laminas\Router',
    'Application',
];
