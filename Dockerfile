FROM registry.access.redhat.com/ubi9/ubi

RUN dnf update -y
RUN dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm https://rpms.remirepo.net/enterprise/remi-release-9.rpm
RUN dnf module enable -y php:remi-7.4 nginx:1.22
RUN dnf install -y nginx sqlite php php-intl composer supervisor zip unzip
RUN dnf clean all

COPY ./containers/nginx.conf /etc/nginx/nginx.conf
COPY ./containers/php.ini /etc/php.ini
COPY ./containers/php-fpm.conf /etc/php-fpm.d/www.conf
COPY ./containers/supervisord.conf /etc/supervisord.conf
COPY ./containers/supervisord.d /etc/supervisord.d
RUN mkdir /run/php-fpm -p

RUN useradd -r httpd -d /srv/http

COPY . /srv/http
RUN chown httpd:nobody -R /srv/http && \
    chown httpd:nobody -R /run/php-fpm
RUN mkdir -p /srv/http/data && \
    chown httpd:nobody -R /srv/http/data && \
    chmod 0770 -R /srv/http/data

USER httpd
WORKDIR /srv/http
VOLUME /srv/http/data
RUN composer install --no-dev

WORKDIR /
USER root

COPY ./containers/entrypoint.sh /entrypoint.sh
CMD /entrypoint.sh