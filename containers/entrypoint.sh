#!/bin/bash

if [ ! -f /srv/http/data/db.sqlite ]; then

  sqlite3 /srv/http/data/db.sqlite < /srv/http/db.sql
  chown httpd:nobody /srv/http/data -R
fi

exec supervisord -c /etc/supervisord.conf